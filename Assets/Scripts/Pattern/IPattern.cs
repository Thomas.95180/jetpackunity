﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPattern {

	void StartPattern();
	void StartCoinPattern();
	float NextWaitDurationBeforeSpawn { get; }
	float NextWaitDurationBeforeSpawnCoin { get; }
	Enemy SpawnEnemy();
	Coin SpawnCoin();
	bool IsPatternOver { get; }
	bool IsCoinPatternOver { get; }
}
