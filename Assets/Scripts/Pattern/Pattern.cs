﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using SDD.Events;

public abstract class Pattern : MonoBehaviour, IPattern,IEventHandler {

	#region IPattern implementation
	public abstract Enemy SpawnEnemy();
	public abstract Coin SpawnCoin();
	public abstract float NextWaitDurationBeforeSpawn { get; }
	public abstract float NextWaitDurationBeforeSpawnCoin { get; }

	public bool IsPatternOver
	{
		get
		{
			return m_NSpawnedEnemies == m_NEnemiesToSpawn
			  && m_Enemies.Count == 0;
		}
	}

	public bool IsCoinPatternOver
	{
		get
		{
			return m_NSpawnedCoins == m_NCoinsToSpawn
			  && m_Coins.Count == 0;
		}
	}

	public virtual void StartPattern()
	{
		StartCoroutine(SpawnCoroutine());
	}
	public virtual void StartCoinPattern()
	{
		StartCoroutine(CoinSpawnCoroutine());
	}
	#endregion

	#region IEventHandler implementation
	public void SubscribeEvents()
	{
		EventManager.Instance.AddListener<EnemyHasBeenDestroyedEvent>(EnemyHasBeenDestroyed);
		EventManager.Instance.AddListener<PlayerGainCoinEvent>(CoinHasBeenTaken);
	}

	public void UnsubscribeEvents()
	{
		EventManager.Instance.RemoveListener<EnemyHasBeenDestroyedEvent>(EnemyHasBeenDestroyed);
		EventManager.Instance.RemoveListener<PlayerGainCoinEvent>(CoinHasBeenTaken);
	}
	#endregion

	#region Monobehaviour Implementation
	private void Awake()
	{
		SubscribeEvents();
	}
	private void OnDestroy()
	{
		UnsubscribeEvents();
	}
	#endregion

	[Header("Pattern")]
	#region enemies spawn
	[SerializeField] protected GameObject m_EnemyPrefab;
	[SerializeField] private int m_NEnemiesToSpawn;
	private int m_NSpawnedEnemies;

	[SerializeField] private float m_WaitDurationAfterLastEnemySpawn=1;
	List<Enemy> m_Enemies = new List<Enemy>();

	IEnumerator SpawnCoroutine()
	{
		m_NSpawnedEnemies = 0;

		while (m_NSpawnedEnemies< m_NEnemiesToSpawn)
		{
			float waitDurationBeforeSpawn = NextWaitDurationBeforeSpawn;
			float elapsedTime = 0;
			while(elapsedTime< waitDurationBeforeSpawn)
			{
				elapsedTime += Time.deltaTime;
				yield return null;
			}
			m_NSpawnedEnemies++;
			Enemy newEnemy = SpawnEnemy();
			newEnemy.transform.SetParent(transform);
			m_Enemies.Add(newEnemy);
		}

		yield return new WaitForSeconds(m_WaitDurationAfterLastEnemySpawn);
		EventManager.Instance.Raise(new PatternHasFinishedSpawningEvent());
		yield break;
	}

	void ClearEnemies()
	{
		foreach (var item in m_Enemies)
		{
			if (item) Destroy(item.gameObject);
		}
		m_Enemies.Clear();
	}
	#endregion

	#region Callbacks to Enemy events
	void EnemyHasBeenDestroyed(EnemyHasBeenDestroyedEvent e)
	{
		m_Enemies.Remove(e.eEnemy);
		if (IsPatternOver)
		{
			EventManager.Instance.Raise(new AllEnemiesOfPatternHaveBeenDestroyedEvent());
		}
	}
	#endregion

	[Header("Pattern")]
	#region coin spawn
	[SerializeField] protected GameObject m_CoinPrefab;
	[SerializeField] private int m_NCoinsToSpawn;
	private int m_NSpawnedCoins;

	[SerializeField] private float m_WaitDurationAfterLastCoinSpawn = 1;
	List<Coin> m_Coins = new List<Coin>();

	IEnumerator CoinSpawnCoroutine()
	{
		m_NSpawnedCoins = 0;

		while (m_NSpawnedCoins < m_NCoinsToSpawn)
		{
			float waitDurationBeforeSpawn = NextWaitDurationBeforeSpawnCoin;
			float elapsedTime = 0;
			while (elapsedTime < waitDurationBeforeSpawn)
			{
				elapsedTime += Time.deltaTime;
				yield return null;
			}
			m_NSpawnedCoins++;
			Coin newCoin = SpawnCoin();
			newCoin.transform.SetParent(transform);
			m_Coins.Add(newCoin);
		}

		yield return new WaitForSeconds(m_WaitDurationAfterLastCoinSpawn);
		EventManager.Instance.Raise(new PatternHasFinishedSpawningEvent());
		yield break;
	}

	void ClearCoins()
	{
		foreach (var item in m_Coins)
		{
			if (item) Destroy(item.gameObject);
		}
		m_Coins.Clear();
	}
	#endregion

	#region Callbacks to Coin events
	void CoinHasBeenTaken(PlayerGainCoinEvent e)
	{
		m_Coins.Remove(e.eCoin);
		if (IsCoinPatternOver)
		{
			EventManager.Instance.Raise(new AllCoinsOfPatternHaveBeenTakenEvent());
		}
	}
	#endregion
	
}
